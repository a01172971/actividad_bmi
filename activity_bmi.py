#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np

#Original DataFrame
data = {'Edad':[19, 26, 22, 40], 'Peso':[60, 55, 85, 80], 'Estatura':[163, 160, 183, 170]}


#Function that:
#1 - Read the dataframe
#2 - Computes the BMI and add its value as a column
#3 - Writes the new dataframe as a csv and returns the dataframe
def df_bmi():
    df = pd.DataFrame(data)
    kg = df['Peso']
    m = df['Estatura'] / 100
    m2 = m*m
    bmi = kg / m2
    df['BMI'] = bmi
    df.to_csv('df1.csv', index=False)
    return df

#Funtion that:
#1 - Receives a dataframe and creates another
#2 - Computes and normalizes the values
#3 - Write the new dataframe in a csv
def normalize(df):
    df2 = df

    #Function inside function normalize() that computes the deviation of the data of any column
    def deviation(column):
        x = df2[column]
        mu = np.mean(df2[column])
        sigma = np.std(df2[column])
        norm = (x-mu)/sigma
        return norm
    
    edad_n = deviation('Edad')
    df2['Edad_norm'] = edad_n

    peso_n = deviation('Peso')
    df2['Peso_norm'] = peso_n

    estatura_n = deviation('Estatura')
    df2['Estatura_norm'] = estatura_n

    bmi_n = deviation('BMI')
    df2['BMI_norm'] = bmi_n
    df2.to_csv('df2.csv', index=False)


#When the script is executed as the main script, it executes function df_bmi() and function normalize()
if __name__ == "__main__":
    y = df_bmi()
    normalize(y)
